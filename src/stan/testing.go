package stan

import (
	"runtime/debug"
	"testing"
)

// AssertEquals asserts if the two objects are the same.
func AssertEquals(t *testing.T, expected interface{}, actual interface{}) {
	if expected != actual {
		debug.PrintStack()
		t.Errorf("Expected: %s, got: %s\n", expected, actual)
	}
}
