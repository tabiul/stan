package stan

import (
	"encoding/json"
	"fmt"
)

// Request defines the json data that is expected from the request
type Request struct {
	PayLoad []struct {
		Drm          bool `json:"drm"`
		EpisodeCount int  `json:"episodeCount"`
		Image        struct {
			ShowImage string `json:"showImage"`
		} `json:"image"`
		Slug  string `json:"slug"`
		Title string `json:"title"`
	} `json:"payLoad"`
}

// Response defines the response structure
type Response struct {
	ResponseData []ResponseData `json:"response"`
}

// ResponseData defines the respose data
type ResponseData struct {
	Image string `json:"image"`
	Slug  string `json:"slug"`
	Title string `json:"title"`
}

// Process reads the request and prepare the response
func Process(data []byte) (Response, error) {
	request, err := parseRequest(data)
	if err != nil {
		return Response{}, fmt.Errorf("Could not decode request: JSON parsing failed")
	}
	responseData := prepareResponse(request)
	return Response{responseData}, nil
}

func parseRequest(data []byte) (*Request, error) {
	request := &Request{}
	err := json.Unmarshal(data, request)
	if err != nil {
		return nil, fmt.Errorf("error unmarshalling the data %s", err)
	}
	return request, nil
}

func prepareResponse(request *Request) []ResponseData {
	responseData := []ResponseData{}
	for _, data := range request.PayLoad {
		if data.Drm && data.EpisodeCount > 0 {
			responseData = append(responseData, ResponseData{
				data.Image.ShowImage,
				data.Slug,
				data.Title,
			})
		}
	}

	return responseData
}
