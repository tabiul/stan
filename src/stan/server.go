package stan

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type errorMessage struct {
	Message string `json:"error"`
}

func handler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		defer r.Body.Close()
		bytes, err := ioutil.ReadAll(r.Body)
		if err != nil {
			buildErrorResponse(w, http.StatusBadRequest, err.Error())
		} else {
			response, err := Process(bytes)
			if err != nil {
				buildErrorResponse(w, http.StatusBadRequest, err.Error())
			} else {
				buildSuccessResponse(w, response)
			}
		}

	} else {
		buildErrorResponse(w, http.StatusMethodNotAllowed, "only POST is allowed")
	}

}

func buildErrorResponse(w http.ResponseWriter, httpStatus int, message string) {
	log.Printf("Error Message: %s", message)
	buildResponse(w, httpStatus, errorMessage{message})
}

func buildSuccessResponse(w http.ResponseWriter, response interface{}) {
	log.Printf("Success Response: %v", response)
	buildResponse(w, http.StatusOK, response)
}

func buildResponse(w http.ResponseWriter, httpStatus int, res interface{}) {
	log.Printf("Response: %v", res)
	bytes, err := json.Marshal(res)
	if err != nil {
		log.Printf("Unable to marshall json %s", err)
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Header().Set("Content-Length", strconv.Itoa(len(bytes)))
		w.WriteHeader(httpStatus)
		w.Write(bytes)
	}
}

// Serve serves all request to `/`
func Serve(port string) {
	http.HandleFunc("/", handler)
	log.Printf("listening to port %s", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}
