package stan

import (
	"io/ioutil"
	"path"
	"testing"
)

func TestValidParseRequest(t *testing.T) {
	file := path.Join("testdata", "request-valid.json")
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		t.Fatalf("error reading file %s %s", file, err)
	}
	request, err := parseRequest(bytes)
	if err != nil {
		t.Fatalf("expected no error, instead there is %s", err)
	}
	AssertEquals(t, 3, len(request.PayLoad))
	AssertEquals(t, "16 Kids and Counting", request.PayLoad[0].Title)
	AssertEquals(t, "show/16kidsandcounting", request.PayLoad[0].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg", request.PayLoad[0].Image.ShowImage)
	AssertEquals(t, "Sea Patrol", request.PayLoad[1].Title)
	AssertEquals(t, "show/seapatrol", request.PayLoad[1].Slug)
	AssertEquals(t, "", request.PayLoad[1].Image.ShowImage)
	AssertEquals(t, "The Taste (Le Goût)", request.PayLoad[2].Title)
	AssertEquals(t, "show/thetaste", request.PayLoad[2].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg", request.PayLoad[2].Image.ShowImage)
}

func TestInvalidParseRequest(t *testing.T) {
	_, err := parseRequest([]byte("invalid json data"))
	if err == nil {
		t.Fatal("expected error but no error occured")
	}

}

func TestPrepareResponse(t *testing.T) {
	file := path.Join("testdata", "request-valid.json")
	bytes, err := ioutil.ReadFile(file)
	if err != nil {
		t.Fatalf("error reading file %s %s", file, err)
	}
	request, err := parseRequest(bytes)
	if err != nil {
		t.Fatalf("expected no error, instead there is %s", err)
	}

	response := prepareResponse(request)
	AssertEquals(t, 2, len(response))
	AssertEquals(t, "16 Kids and Counting", response[0].Title)
	AssertEquals(t, "show/16kidsandcounting", response[0].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg", response[0].Image)
	AssertEquals(t, "The Taste (Le Goût)", response[1].Title)
	AssertEquals(t, "show/thetaste", response[1].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg", response[1].Image)

}
