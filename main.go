package main

import (
	"log"
	"os"
	"stan"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		log.Fatal("$PORT must be set")
	}
	stan.Serve(port)
}

