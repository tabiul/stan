package stan

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"path"
	"testing"
)

type mockResponseWriter struct {
	HttpStatus int
	bytes      []byte
	header     http.Header
}

func newMockResponseWriter() *mockResponseWriter {
	mock := mockResponseWriter{}
	mock.header = make(http.Header)
	return &mock
}

func (mock *mockResponseWriter) Header() http.Header {
	return mock.header
}

func (mock *mockResponseWriter) Write(bytes []byte) (int, error) {
	mock.bytes = bytes
	return len(bytes), nil

}

func (mock *mockResponseWriter) WriteHeader(statusCode int) {
	mock.HttpStatus = statusCode
}

func TestInvalidHttMethod(t *testing.T) {

	request := http.Request{}
	request.Method = "GET"
	mock := newMockResponseWriter()
	handler(mock, &request)
	AssertEquals(t, http.StatusMethodNotAllowed, mock.HttpStatus)
}

func TestInvalidRequest(t *testing.T) {
	request := http.Request{}
	request.Method = "POST"
	request.Body = ioutil.NopCloser(bytes.NewReader([]byte("Invalid JSON")))
	mock := newMockResponseWriter()
	handler(mock, &request)
	AssertEquals(t, http.StatusBadRequest, mock.HttpStatus)
	errorMessage := &errorMessage{}
	err := json.Unmarshal(mock.bytes, &errorMessage)
	if err != nil {
		t.Fatalf("error in parsing json response %s", err)
	}
	AssertEquals(t, http.StatusBadRequest, mock.HttpStatus)
	AssertEquals(t, "application/json", mock.header.Get("content-type"))
	AssertEquals(t, "Could not decode request: JSON parsing failed", errorMessage.Message)
}

func TestValidRequest(t *testing.T) {
	request := http.Request{}
	request.Method = "POST"

	file := path.Join("testdata", "request-valid.json")
	data, err := ioutil.ReadFile(file)
	if err != nil {
		t.Fatalf("error reading file %s %s", file, err)
	}

	request.Body = ioutil.NopCloser(bytes.NewReader(data))
	mock := newMockResponseWriter()
	handler(mock, &request)
	response := &Response{}
	err = json.Unmarshal(mock.bytes, &response)
	if err != nil {
		t.Fatalf("error in parsing json response %s", err)
	}
	AssertEquals(t, http.StatusOK, mock.HttpStatus)
	AssertEquals(t, "application/json", mock.header.Get("content-type"))
	AssertEquals(t, 2, len(response.ResponseData))
	AssertEquals(t, "16 Kids and Counting", response.ResponseData[0].Title)
	AssertEquals(t, "show/16kidsandcounting", response.ResponseData[0].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/16KidsandCounting1280.jpg", response.ResponseData[0].Image)
	AssertEquals(t, "The Taste (Le Goût)", response.ResponseData[1].Title)
	AssertEquals(t, "show/thetaste", response.ResponseData[1].Slug)
	AssertEquals(t, "http://catchup.ninemsn.com.au/img/jump-in/shows/TheTaste1280.jpg", response.ResponseData[1].Image)
}
